MPIFLAGS    = -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/siserte/slurm/include -L/home/siserte/slurm/lib -lslurm
#MKLFLAGS    = -I/apps/INTEL/composerxe/mkl/include -L/apps/INTEL/composerxe/mkl/lib -I/apps/OPENBLAS/0.2.14-openmp/GCC/OPENMPI/include -L/apps/OPENBLAS/0.2.14-openmp/GCC/OPENMPI/lib -lmkl_gf_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm
MKLFLAGS    = -I/state/partition1/soft/intel/mkl/include -L/state/partition1/soft/intel/mkl/lib/intel64 -I/state/partition1/soft/gnu/OpenBLAS-0.2.19/include -L/state/partition1/soft/gnu/OpenBLAS-0.2.19/lib -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm

#MPIFLAGS    = -I/home/bsc15/bsc15334/apps/install/mpich-3.2/include -L/home/bsc15/bsc15334/apps/install/mpich-3.2/lib -lmpi
#SLURMFLAGS  = -I/home/bsc15/bsc15334/apps/install/slurm/include -L/home/bsc15/bsc15334/apps/install/slurm/lib -lslurm
#MKLFLAGS    = -I/apps/INTEL/composerxe/mkl/include  ${MKLROOT}/lib/intel64/libmkl_scalapack_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_gnu_thread.a ${MKLROOT}/lib/intel64/libmkl_core.a ${MKLROOT}/lib/intel64/libmkl_blacs_openmpi_lp64.a -Wl,--end-group -lgomp -lpthread -lm -ldl

OMPSSFLAGS  = -O3 -k --ompss

LDFLAGS = -nofor_main
LDFLAGS = 


#all: cg_malleable cg_orig cg_moldable
all: cg_malleable

print-%  : ; @echo $* = $($*)

cg_moldable: cg-ompss-moldable.c
	mpimcc $(OMPSSFLAGS) cg-ompss-moldable.c $(MPIFLAGS) $(SLURMFLAGS) $(MKLFLAGS) $(LDFLAGS) -fopenmp -o cg-ompss-moldable.INTEL64

cg_malleable: cg-ompss.c
	mpimcc $(OMPSSFLAGS) cg-ompss.c $(MPIFLAGS) $(SLURMFLAGS) $(MKLFLAGS) $(LDFLAGS) -fopenmp -o cg-ompss.INTEL64

cg_orig: cg-orig.c
	mpicc -std=gnu99 cg-orig.c $(MPIFLAGS) $(MKLFLAGS) $(LDFLAGS) -fopenmp -o cg-orig.INTEL64

clean:
	/bin/rm -rf core.* *.o *.INTEL64 mpimcc_mcc*

