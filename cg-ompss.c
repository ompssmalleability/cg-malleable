#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mkl_blas.h>
#include <mpi.h>
#include "reloj.c"

double wall_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double) (ts.tv_sec) + (double) ts.tv_nsec * 1.0e-9;
}

#define DEBUG 1
#define MALLEABLE 1
#define OPENMP 1
#define MAXITER 10000
#define SIZE 32768
//#define SIZE 65536 -> no cabe en memoria!!!
//#define SIZE 16384
#define SIZE 8096
#define MIN 1
#define MAX 8

void CreateDoubles(double **vdouble, int size) {
    if ((*vdouble = (double *) malloc(sizeof (double)*size)) == NULL) {
        printf("Memory Error (CreateDoubles(%d))\n", size);
        exit(1);
    }
}

void RemoveDoubles(double **vdouble) {
    free(*vdouble);
    *vdouble = NULL;
}

void InitDoubles(double *vdouble, int n, double frst, double incr) {
    int i;
    double *p1 = vdouble, num = frst;

    for (i = 0; i < n; i++) {
        *(p1++) = num;
        num += incr;
    }
}

// ================================================================================
// ========================= FLAT FUNCTIONS =======================================
// ================================================================================

void GenerateDistributeMatrixFlat(double **pmat, int sizeR, int sizeC) {
    const int max = 10, off = 0, div = 5000;
    int i;
    double *mat = NULL;

    mat = (double *) malloc(sizeR * sizeC * sizeof (double));

    for (i = 0; i < sizeR * sizeC; i++)
        mat[i] = ((rand() % max) - off) / (1.0 * div);

    *pmat = mat;
}

void ProdMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *x, double *b) {
    int i, j;
    double sum;

    if (OPENMP) {
#pragma omp parallel for private(j, sum)
        for (i = 0; i < sizeR; i++) {
            sum = 0.0;
            for (j = 0; j < sizeC; j++) {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    } else {
        for (i = 0; i < sizeR; i++) {
            sum = 0.0;
            for (j = 0; j < sizeC; j++) {
                sum += mat[sizeC * i + j] * x[j];
            }
            b[i] = sum;
        }
    }
}

void AddMatrixVectorFlat(double *mat, int sizeR, int sizeC, double *b) {
    int i, j;
    double sum;

    for (i = 0; i < sizeR; i++) {
        sum = 0.0;
        for (j = 0; j < sizeC; j++) {
            sum += mat[sizeC * i + j];
        }
        b[i] = sum;
    }

}

// ================================================================================
// ================================================================================
// ================================================================================

void ConjugateGradient(double *mat, double *x,
        double* res, double *d, double *z, int iter) {
    //sleep(2);
    int i;
    int IZERO = 0, IONE = 1;
    double DONE = 1.0, DMONE = -1.0, DZERO = 0.0;
    int n, n_dist, maxiter = MAXITER;
    double tol, rho, alpha, umbral;
    //double *res = NULL, *z = NULL, *d = NULL, *y = NULL;
    double *y = NULL, *aux = NULL;
    double t1, t2, t3, t4;
    double p1, p2, p3, p4, pp1 = 0.0, pp2 = 0.0;

    int myId, numProcs;
    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);

    int size = SIZE;
    int sizeC = size;
    int sizeR = size / numProcs;

    const double start = wall_time();
    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - Size: %d, Size per process: %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, size, sizeR);
    CreateDoubles(&aux, size);
    int *sizes = (int *) malloc(numProcs * sizeof (int));
    int *dspls = (int *) malloc(numProcs * sizeof (int));
    MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
    dspls[0] = 0;
    for (i = 1; i < numProcs; i++) dspls[i] = dspls[i - 1] + sizes[i - 1];
    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);

    n = size;
    n_dist = sizeR;
    umbral = 1.0e-8; // maxiter = size;
    // tol = norm (res)
    y = res;
    double beta = ddot(&n_dist, res, &IONE, y, &IONE); // beta = res' * y
    MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
    tol = sqrt(beta);
    if (iter < maxiter) {
        MPI_Allgatherv(d, sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, MPI_COMM_WORLD);
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        ProdMatrixVectorFlat(mat, sizeR, sizeC, aux, z); // z = A * d
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        //if (myId == 0) printf("(%d,%20.10e)\n", iter, tol);
        rho = ddot(&n_dist, d, &IONE, z, &IONE);
        MPI_Allreduce(MPI_IN_PLACE, &rho, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        rho = beta / rho;
        daxpy(&n_dist, &rho, d, &IONE, x, &IONE); // x += rho * d;
        rho = -rho;
        daxpy(&n_dist, &rho, z, &IONE, res, &IONE); // res -= rho * z
        //		dcopy (&n_dist, res, &IONE, y, &IONE);                   				// y = res
        y = res;
        alpha = beta; // alpha = beta
        //		beta = ddot (&n_dist, res, &IONE, y, &IONE);                    // beta = res' * y
        beta = ddot(&n_dist, res, &IONE, y, &IONE);
        MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        alpha = beta / alpha; // alpha = beta / alpha
        dscal(&n_dist, &alpha, d, &IONE); // d = alpha * d
        daxpy(&n_dist, &DONE, y, &IONE, d, &IONE); // d += y
        //		tol = dnrm2 (&n_dist, res, &IONE);                              // tol = norm (res)
        //tol = sqrt(*beta);
        iter++;
    } else {
        return;
    }
    RemoveDoubles(&aux);

    int action = 0;
    MPI_Comm booster;
    int nnodes;
    const double end = wall_time();
    if (DEBUG && myId == 0)
        printf("TIME 1: Step %d: with %d nodes %g seconds.\n", iter, numProcs, end - start);
    /*********************************************************************************************************/
    if (MALLEABLE)
        action = dmr_check_status(MIN, MAX, 2, 0, &nnodes, &booster);
    const double endRMS = wall_time();
    if (MALLEABLE)
        if (myId == 0)
            printf("TIME 2: Resize of step %d: from %d to %d in %g seconds.\n", iter, numProcs, nnodes, endRMS - end);
    /*********************************************************************************************************/
    //if (myId == 0)
    //    printf("[%d/%d] TIMES:\n\tClock before DMR %g\n\tClock after DMR %g\n", iter, myId, numProcs, end, endRMS);
    if (action <= 0) {
        ConjugateGradient(mat, x, res, d, z, iter);
    } else if (action == 1) {
        int factor = nnodes / numProcs;
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - %d %d %d %d %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, nnodes, numProcs, factor, sizeR, i);
        //printf("(sergio)[%d/%d] %d: %s(%s,%d) %d %d - %p %p %p %p %p %p\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, iniPart, finPart, mat, x, res, d, z, aux);
        for (i = 0; i < factor; i++) {
            int dst = myId * factor + i;
            int iniPart = (sizeR / factor) * i;
            int finPart = ((sizeR / factor) * (i + 1));
            int iniMat = ((sizeC * sizeR) / factor) * i;
            int finMat = (((sizeC * sizeR) / factor) * (i + 1));
            //printf("(sergio)[%d/%d] %d: %s(%s,%d) %d %d %d %d- %p %p %p %p %p %p\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, iniPart, finPart, iniMat, finMat, mat, x, res, d, z, aux);
#pragma omp task in(mat[iniMat:finMat], x[iniPart:finPart], res[iniPart:finPart], d[iniPart:finPart], z[iniPart:finPart]) onto(booster, dst)
            ConjugateGradient(mat + iniMat, x + iniPart, res + iniPart, d + iniPart, z + iniPart, iter);
        }
#pragma omp taskwait
    } else if (action == 2) {
        int factor = numProcs / nnodes;
        int dst;
        MPI_Request sendRequest;
        MPI_Status statuses[factor];
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        int newSize = sizeR * factor;
        double *newRes, *newZ, *newD, *newX, *newMat, *ptr;
        CreateDoubles(&newRes, newSize);
        CreateDoubles(&newZ, newSize);
        CreateDoubles(&newD, newSize);
        CreateDoubles(&newX, newSize);
        CreateDoubles(&newMat, sizeC * newSize);
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) %d %d %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, nnodes, numProcs, factor);
        if ((myId % factor) < (factor - 1)) { //sender
            dst = factor * (myId / factor + 1) - 1;
            //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - dst %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, dst);
            MPI_Isend(res, sizeR, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(z, sizeR, MPI_DOUBLE, dst, 1, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(d, sizeR, MPI_DOUBLE, dst, 2, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(x, sizeR, MPI_DOUBLE, dst, 3, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(mat, sizeC * sizeR, MPI_DOUBLE, dst, 4, MPI_COMM_WORLD, &sendRequest);
        } else { //receiver
            //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
            dst = myId;
            MPI_Isend(res, sizeR, MPI_DOUBLE, dst, 0, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(z, sizeR, MPI_DOUBLE, dst, 1, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(d, sizeR, MPI_DOUBLE, dst, 2, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(x, sizeR, MPI_DOUBLE, dst, 3, MPI_COMM_WORLD, &sendRequest);
            MPI_Isend(mat, sizeC * sizeR, MPI_DOUBLE, dst, 4, MPI_COMM_WORLD, &sendRequest);
            for (int i = 1; i <= factor; i++) {
                int src = myId - factor + i;
                //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - src %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, src);
                ptr = newRes + ((i - 1) * sizeR);
                MPI_Recv(ptr, sizeR, MPI_DOUBLE, src, 0, MPI_COMM_WORLD, &statuses[i - 1]);
                ptr = newZ + ((i - 1) * sizeR);
                MPI_Recv(ptr, sizeR, MPI_DOUBLE, src, 1, MPI_COMM_WORLD, &statuses[i - 1]);
                ptr = newD + ((i - 1) * sizeR);
                MPI_Recv(ptr, sizeR, MPI_DOUBLE, src, 2, MPI_COMM_WORLD, &statuses[i - 1]);
                ptr = newX + ((i - 1) * sizeR);
                MPI_Recv(ptr, sizeR, MPI_DOUBLE, src, 3, MPI_COMM_WORLD, &statuses[i - 1]);
                ptr = newMat + ((i - 1) * sizeR * sizeC);
                MPI_Recv(ptr, sizeC * sizeR, MPI_DOUBLE, src, 4, MPI_COMM_WORLD, &statuses[i - 1]);
            }
        }
        //printf("(sergio)[%d/%d] %d: %s(%s,%d)\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__);
        MPI_Barrier(MPI_COMM_WORLD);

        int newSizeMat = sizeR * sizeC * factor;
        if ((myId % factor) < (factor - 1)) {
        } else {
            dst = myId / factor;
            //printf("(sergio)[%d/%d] %d: %s(%s,%d) sizeMat %d, sizeVec %d\n", myId, numProcs, getpid(), __FILE__, __func__, __LINE__, newSizeMat, newSize);
#pragma omp task in(newMat[0:newSizeMat-1], newX[0:newSize-1], newRes[0:newSize-1], newD[0:newSize-1], newZ[0:newSize-1]) onto(booster, dst)
            ConjugateGradient(newMat, newX, newRes, newD, newZ, iter);
#pragma omp taskwait
        }
    }

    //	RemoveDoubles (&aux); RemoveDoubles (&y); RemoveDoubles (&res); RemoveDoubles (&z); RemoveDoubles (&d);
    //RemoveDoubles(&aux);
    //RemoveDoubles(&res);
    //RemoveDoubles(&z);
    //RemoveDoubles(&d);
}

int main(int argc, char *argv[]) {
    int i, val, ret = 0;
    //	int *posd = NULL;
    double norm = 0.0;
    //	double *x = NULL, *b1 = NULL, *b2 = NULL, *diags = NULL;
    double *x = NULL, *b = NULL;
    double *mat = NULL;
    //	SparseMatrix mat, sym;

    int myId, numProcs;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &myId);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    printf("Process %d of %d\n", myId, numProcs);

    srand(myId + 1);

    //if (argc != 2) {
    //	printf ("ERROR: CG_Def size \n"); res = -1;
    //} else {
    int IONE = 1;
    double beta = 0.0;

    int size, sizeR, sizeC;
    int *sizes = NULL, *dspls = NULL;
    size = SIZE;
    //size = atoi (argv[1]);
    //		sizeR = (size + (numProcs - 1)) / numProcs;
    sizeR = size / numProcs;
    if ((size % numProcs) > myId) {
        sizeR++;
    }
    //		printf ("sizeR %d\n", sizeR);
    sizeC = size;
    sizes = (int *) malloc(numProcs * sizeof (int));
    dspls = (int *) malloc(numProcs * sizeof (int));
    MPI_Allgather(&sizeR, 1, MPI_INT, sizes, 1, MPI_INT, MPI_COMM_WORLD);
    dspls[0] = 0;
    for (i = 1; i < numProcs; i++) dspls[i] = dspls[i - 1] + sizes[i - 1];

    //	Begin of CG
    GenerateDistributeMatrixFlat(&mat, sizeR, sizeC);

    double *aux, *res, *z, *d;
    CreateDoubles(&aux, size);
    CreateDoubles(&res, sizeR);
    CreateDoubles(&z, sizeR);
    CreateDoubles(&d, sizeR); // CreateDoubles (&y, n_dist);

    CreateDoubles(&x, sizeR);
    CreateDoubles(&b, sizeR);
    InitDoubles(x, sizeR, 0.0, 0.0);
    InitDoubles(b, sizeR, 0.0, 0.0);

    // A*[1,1, ... 1]'= b
    AddMatrixVectorFlat(mat, sizeR, sizeC, b);
    // Transform A as a dominant diagonal matrix, the less dominant, the less likely to converge, so increase the divider (100)
    for (i = 0; i < sizeR; i++) {
        mat[sizeC * i + i + dspls[myId]] += b[i] / 100;
        b[i] += b[i] / 100;
    }

    double DMONE = -1.0;
    int n_dist = sizeR;
    MPI_Allgatherv(x, sizeR, MPI_DOUBLE, aux, sizes, dspls, MPI_DOUBLE, MPI_COMM_WORLD);
    ProdMatrixVectorFlat(mat, sizeR, sizeC, aux, z);
    dcopy(&n_dist, b, &IONE, res, &IONE); // res = b
    daxpy(&n_dist, &DMONE, z, &IONE, res, &IONE); // res -= z
    dcopy(&n_dist, res, &IONE, d, &IONE); // d = y
    //beta = ddot(&n_dist, res, &IONE, res, &IONE); // beta = res' * y
    //MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    ConjugateGradient(mat, x, res, d, z, 0);
    //int tol = sqrt(beta);
    //if (myId == 0)
    //    printf("Fin(%d) --> (%d,%20.10e)\n", size, MAXITER, tol);


    // Error computation
    for (i = 0; i < sizeR; i++) x[i] -= 1.0;
    beta = ddot(&sizeR, x, &IONE, x, &IONE);
    MPI_Allreduce(MPI_IN_PLACE, &beta, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    beta = sqrt(beta);
    if (myId == 0) printf("error = %10.5e\n", beta);

    RemoveDoubles(&aux);
    RemoveDoubles(&res);
    RemoveDoubles(&d);
    RemoveDoubles(&z);

    RemoveDoubles(&b);
    RemoveDoubles(&x);
    RemoveDoubles(&mat);
    //RemoveMatrixDoubles(&mat);
    //	End of CG
    free(sizes);
    sizes = NULL;
    //}

    MPI_Finalize();

    return ret;
}

